from typing import List, Union
import os
import math

import pandas as pd


cur_dir = os.path.dirname(os.path.abspath(__file__))


def make_anki_importable(file_path: str) -> List[List[str]]:

    NumOrStr = Union[str, float, int]

    def is_nan(val: NumOrStr) -> bool:
        if not hasattr(val, "format") and math.isnan(val):
            return True
        return False

    def make_rows(eng: NumOrStr, mar: NumOrStr, tag: NumOrStr) -> List[str]:
        eng = eng.strip()
        mar = mar.strip()
        row = [eng, mar]
        if is_nan(tag):
            tag = ""
        row.append(tag)
        yield row
        # row = row.copy()
        # row[0] = mar
        # row[1] = eng
        # yield row

    df = pd.read_excel(file_path)
    result = []

    for _, row in df.iterrows():
        # print(row)
        eng, mar, tag = row["English"], row["Marathi"], row["Notes"]
        if is_nan(mar):
            continue
        else:
            if "/" in mar:
                for idx, _mar in enumerate(mar.split("/")):
                    for row in make_rows(f"{eng} ({idx + 1})", _mar, tag):
                        result.append(row)
            else:
                for row in make_rows(eng, mar, tag):
                    result.append(row)

    return result


def main():
    vocab_file_path = os.path.join(cur_dir, "vocab.xlsx")
    res = make_anki_importable(vocab_file_path)

    with open("vocab.txt", "w") as fd:
        for row in res:
            fd.write(f'{";".join(row)}\n')


if __name__ == "__main__":
    main()
